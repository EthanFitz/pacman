using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioController : MonoBehaviour
{
  public AudioClip introMusic;
  public AudioClip[] backgroundMusic; //0 for normal gameplay, 1 for scared ghosts, 2 for at least 1 dead ghost

  AudioSource speakers;
  int currentSpeed = -1; //-1 for intro, 0-3 for respective background music
  bool introPlaying = true;

  void Start(){
    speakers = GetComponent<AudioSource>();
    Invoke("OnIntroComplete", speakers.clip.length);
  }

  void OnIntroComplete(){
    introPlaying = false;
    ChangeTempo(0);
    speakers.loop = true;
  }

  public void ChangeTempo(int speed){
    if (currentSpeed != speed && !introPlaying){
      float newTimecode = (speakers.time / speakers.clip.length) * backgroundMusic[speed].length; //basic ratio conversion (t/l = t/l)
      speakers.Stop();
      speakers.clip = backgroundMusic[speed];
      speakers.time = newTimecode;
      speakers.Play();
      currentSpeed = speed;
    }
  }
}
