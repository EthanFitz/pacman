using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PacManController : MonoBehaviour
{
  public float tweenDemoCycleLength;

  Vector3[] positions = {
    new Vector3(-13f, 13f, 0f),
    new Vector3( -8f, 13f, 0f),
    new Vector3( -8f,  9f, 0f),
    new Vector3(-13f,  9f, 0f)
  };
  AudioSource speakers;
  int animationCounter = 0;
  int legCounter = 1;
  float tweenTimer = 0.0f;

  void Start(){
    speakers = this.gameObject.GetComponent<AudioSource>();
  }

  // Update is called once per frame
  void Update(){
    UpdateMovement();
  }

  void UpdateMovement(){
    tweenTimer += Time.deltaTime;
    while (tweenTimer > tweenDemoCycleLength){
      tweenTimer -= tweenDemoCycleLength;
    }

    CheckScale();

    if (Vector3.Distance(this.gameObject.transform.position, GetEnd(legCounter)) > 0.1f){
      float timeFraction = tweenTimer / (tweenDemoCycleLength / 18f * GetLegLength(legCounter));
      transform.position = Vector3.Lerp(GetStart(legCounter), GetEnd(legCounter), timeFraction);
    } else {
      transform.position = GetEnd(legCounter);
      tweenTimer = 0.0f;
      IncreaseLegCounter();
    }
  }

  void IncreaseLegCounter(){
    if (legCounter == 4){
      legCounter = 1;
    } else {
      legCounter++;
    }
  }

  void CheckScale(){
    if (legCounter == 1 || legCounter == 4){
      if (transform.localScale.x < 0){
        InvertScale();
      }
    } else {
      if (transform.localScale.x > 0){
        InvertScale();
      }
    }
  }

  float GetLegLength(int leg){
    if (IsEven(leg)){
      return 4f;
    } else {
      return 5f;
    }
  }

  Vector3 GetStart(int leg){
    return positions[leg - 1];
  }

  Vector3 GetEnd(int leg){
    if (leg == 4){
      return positions[0];
    } else {
      return positions[leg];
    }
  }

  bool IsEven(int number){
    return (number % 2 == 0);
  }

  void ManageScale(){
    animationCounter++;
    if (animationCounter >= 5){
      InvertScale();
      animationCounter = 0;
    }
  }

  void InvertScale(){
    transform.localScale = new Vector3(transform.localScale.x * -1f, transform.localScale.y, transform.localScale.z);
  }

  void PlayStepSound(){
    speakers.Play();
  }

  void OnIdleComplete(){
    ManageScale();
  }

  void OnRunningStart(){
    PlayStepSound();
  }

  void OnRunningMid(){
    PlayStepSound();
  }

  void OnRunningComplete(){
    ManageScale();
  }
}
